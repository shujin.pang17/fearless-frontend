window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
        // Create an 'option' element
        const option_el = document.createElement('option')
        option_el.value = conference.id
        option_el.innerHTML = conference.name
        // Append the option element as a child of the select tag
        selectTag.appendChild(option_el)
      }}

      //submit form
      //select form and add eventlistener for submit
      const formTag = document.getElementById('create-presentation-form');
      formTag.addEventListener("submit", async event =>{
        event.preventDefault();
     // collect form data and turn it into json
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
     //post requet
        const select = document.getElementById("conference");
        let conferenceId = select.value
        const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newPresentation = await response.json();
        console.log(newPresentation);
        }

      })
    })

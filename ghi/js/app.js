
function createCard(name, description, pictureUrl, dateRange, location) {

return `
    <div class="col-sm-11 mb-3 mb-sm-2">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top" alt="Conference Image">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>

        <ul class="list-group list-group-flush">
        <li class="list-group-item">${location}</li>
        </ul>

        <div class="card-footer text-body-secondary">
        ${dateRange}
        </div>
      </div>
    </div>
  `;
}

// return `
//     <div class="col-md-6 col-lg-4 mb-4">
//       <div class="card shadow">
//         <img src="${pictureUrl}" class="card-img-top" alt="Conference Image">
//         <div class="card-body">
//           <h5 class="card-title">${name}</h5>
//           <p class="card-text">${description}</p>
//         </div>
//       </div>
//     </div>
//   `;
// }



// window.addEventListener('DOMContentLoaded', async () => {

//     const url = 'http://localhost:8000/api/conferences/';

//     try {
//       const response = await fetch(url);

//       if (!response.ok) {
//         // Figure out what to do when the response is bad
//       } else {
//     //     const data = await response.json();

//     //     const conference = data.conferences[0];
//     //     const nameTag = document.querySelector('.card-title');
//     //     const descriptionTag = document.querySelector('.card-text');
//     //     const imageTag = document.querySelector('.card-img-top');
//     //     nameTag.innerHTML = conference.name;


//     //     const detailUrl = `http://localhost:8000${conference.href}`;
//     //     const detailResponse = await fetch(detailUrl);
//     //     if (detailResponse.ok) {
//     //       const details = await detailResponse.json();
//     //       descriptionTag.innerHTML = details.conference.description;
//     //       imageTag.src = details.conference.picture_url;
//     //       console.log(details)
//     //       console.log(details.conference.description);

//     //     }

//     //   }
//     const data = await response.json();

//       for (let conference of data.conferences) {
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         const detailResponse = await fetch(detailUrl);
//         if (detailResponse.ok) {
//           const details = await detailResponse.json();
//           const title = details.conference.title;
//           const description = details.conference.description;
//           const pictureUrl = details.conference.location.picture_url;
//           const html = createCard(title, description, pictureUrl);
//           console.log(html);
//         }
//       }

//     }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }

//   });

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        const columns = document.querySelectorAll('.col');
        console.log(columns);
        let columnIndex = 0;

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const location = details.conference.location.name;
            const start = new Date(details.conference.starts).toLocaleDateString('en-US', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });
              const end = new Date(details.conference.ends).toLocaleDateString('en-US', {
                year: 'numeric',
                month: '2-digit',
                day: '2-digit'
              });
            const dateRange = `${start} - ${end}`;
            const html = createCard(title, description, pictureUrl, dateRange, location);
            // console.log(html);
            const column = columns[columnIndex];
            console.log(column);
            column.innerHTML += html;
            columnIndex = (columnIndex + 1) % columns.length
          }
        }

      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.error('An error occurred')
      alert('An error occurred');
    }

  });


window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingIcon = document.getElementById('loading-conference-spinner');
    const formTag = document.getElementById('create-attendee-form');
    const successMessage = document.getElementById('success-message');



    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }
      // Here, add the 'd-none' class to the loading icon
      loadingIcon.classList.add('d-none');
     // Here, remove the 'd-none' class from the select tag
     selectTag.classList.remove('d-none');

    }
    formTag.addEventListener("submit", async event =>{
        event.preventDefault();
     // collect form data and turn it into json
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
     //post requet
        const conferenceUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newAttend = await response.json();
        console.log(newAttend);
        successMessage.classList.remove('d-none');
        formTag.classList.add('d-none');

        }

      })

  });



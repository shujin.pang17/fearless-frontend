
window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('location');
      for (let location of data.locations) {
        // Create an 'option' element
        const option_el = document.createElement('option')
        option_el.value = location.id
        option_el.innerHTML = location.name
        // Append the option element as a child of the select tag
        selectTag.appendChild(option_el)
      }}

      //submit form
      //select form and add eventlistener for submit
      const formTag = document.getElementById('create-conference-form');
      formTag.addEventListener("submit", async event =>{
        event.preventDefault();
     // collect form data and turn it into json
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        console.log(json);
     //post requet
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
        formTag.reset();
        const newConference = await response.json();
        console.log(newConference);
        }

      })
    })

